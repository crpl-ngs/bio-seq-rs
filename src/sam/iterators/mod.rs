use std::io::BufRead;


pub struct SAMEntry {
	pub qname: String,
	pub flag: i32,
	pub rname: String,
	pub pos: i32,
	pub mapq: i32,
	pub cigar: String,
	pub rnext: String,
	pub pnext: i32,
	pub tlen: i32,
	pub seq: String,
	pub qual: String,
	pub optional: String,
}

impl SAMEntry {
	pub fn new(qname: &str, flag: i32, rname: &str, pos: i32, mapq: i32, cigar: &str, rnext: &str, pnext: i32, tlen: i32, seq: &str, qual: &str, optional: &str) -> SAMEntry {
		let qname = qname.to_owned();
		let rname = rname.to_owned();
		let cigar = cigar.to_owned();
		let rnext = rnext.to_owned();
		let seq = seq.to_owned();
		let qual = qual.to_owned();
		let optional = optional.to_owned();

		SAMEntry { qname, flag, rname, pos, mapq, cigar, rnext, pnext, tlen, seq, qual, optional }
	}
}

impl ToString for SAMEntry {
	fn to_string(&self) -> String {
		format!("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}",
		        self.qname, self.flag, self.rname, self.pos, self.mapq, self.cigar, self.rnext, self.pnext, self.tlen, self.seq, self.qual, self.optional)
	}
}

pub struct SAMIterator {
	header: String,
	reader: Box<dyn BufRead>,
	line_buffer: Option<String>,
}

impl SAMIterator {
	pub fn new(reader: Box<dyn BufRead>) -> SAMIterator {
		SAMIterator { reader, header: String::new(), line_buffer: None }
	}

	pub fn get_header(&self) -> &str {
		&self.header
	}
}

impl Iterator for SAMIterator {
	type Item = SAMEntry;

	#[inline]
	fn next(&mut self) -> Option<Self::Item> {
		let line;
		if self.line_buffer.is_none() {
			let mut l = String::new();
			loop {
				match self.reader.read_line(&mut l) {
					Ok(0) => {
						return None;
					},
					Ok(_) => {
						if l.starts_with('@') {
							self.header.push_str(&l);
							l.clear();
						} else {
							line = l.trim().to_owned();
							break;
						}
					},
					Err(_) => {
						return None;
					},
				}
			}
		} else {
			line = self.line_buffer.as_ref().unwrap().trim().to_owned();
			self.line_buffer = None;
		}

		let mut iter = line.split('\t');

		let qname = iter.next().unwrap().to_owned();
		let flag = iter.next().unwrap().parse::<i32>().unwrap();
		let rname = iter.next().unwrap().to_owned();
		let pos = iter.next().unwrap().parse::<i32>().unwrap();
		let mapq = iter.next().unwrap().parse::<i32>().unwrap();
		let cigar = iter.next().unwrap().to_owned();
		let rnext = iter.next().unwrap().to_owned();
		let pnext = iter.next().unwrap().parse::<i32>().unwrap();
		let tlen = iter.next().unwrap().parse::<i32>().unwrap();
		let seq = iter.next().unwrap().to_owned();
		let qual = iter.next().unwrap().to_owned();
		let opt = match iter.next() {
			Some(opt) => opt.to_owned(),
			None => "".to_owned(),
		};

		Some(SAMEntry { qname, flag, rname, pos, mapq, cigar, rnext, pnext, tlen, seq, qual, optional: opt })
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::sequence::get_reader;

	#[test]
	fn test_sam_iter() {
		let iter = SAMIterator::new(get_reader("run/DS100-1.sam.gz"));

		for (i, sam) in iter.enumerate() {
//			println!("{}", sam.to_string());
			if i < 4 {
				println!("{}", sam.to_string());
			} else {
				break;
			}
		}
	}
}