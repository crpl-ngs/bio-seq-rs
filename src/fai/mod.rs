use std::io::BufRead;

#[derive(Debug)]
pub struct FAIEntry {
	pub name: String,
	pub length: u64,
	pub offset: u64,
	pub linebases: u32,
	pub linewidth: u32,
	pub qualoffset: Option<u64>,
}

impl ToString for FAIEntry {
	fn to_string(&self) -> String {
		let s = format!("{}\t{}\t{}\t{}\t{}", self.name, self.length, self.offset, self.linebases, self.linewidth);
		if self.qualoffset.is_none() {
			s
		} else {
			format!("{}\t{}", s, self.qualoffset.unwrap())
		}
	}
}

pub struct FAIIterator {
	line: Option<String>,
	reader: Box<dyn BufRead>,
}

impl FAIIterator {
	pub fn new(reader: Box<dyn BufRead>) -> FAIIterator {
		FAIIterator { line: None, reader }
	}
}

impl std::iter::Iterator for FAIIterator {
	type Item = FAIEntry;

	fn next(&mut self) -> Option<Self::Item> {
		let mut line = String::new();
		match self.reader.read_line(&mut line) {
			Err(_) => None,
			Ok(_) => {
				if line.trim().is_empty() {
					return None;
				}
				let mut iter = line.split('\t');
				let name = iter.next().unwrap().trim().to_owned();
				let length = iter.next().unwrap().trim().parse::<u64>().unwrap();
				let offset = iter.next().unwrap().trim().parse::<u64>().unwrap();
				let linebases = iter.next().unwrap().trim().parse::<u32>().unwrap();
				let linewidth = iter.next().unwrap().trim().parse::<u32>().unwrap();
				let qualoffset = match iter.next() {
					None => None,
					Some(s) => Some(s.trim().parse::<u64>().unwrap()),
				};
				Some(FAIEntry {name, length, offset, linebases, linewidth, qualoffset})
			}
		}
	}
}

#[cfg(test)]
mod test {
	use crate::fai::FAIIterator;

	#[test]
	fn test_fai_iterator() {
		let reader = std::io::BufReader::new(std::fs::File::open("run/hg38.fa.fai").unwrap());
		for entry in FAIIterator::new(Box::new(reader)) {
			println!("{}", entry.to_string());
		}
	}
}