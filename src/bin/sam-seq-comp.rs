
fn main() {
	let args = parse_args();
	run(&args);
}

fn run(args: &Args) {
	use bio_seq_rs::sam::iterators::SAMIterator;
	use bio_seq_rs::sequence::BufferedSequenceIterator;
	use bio_seq_rs::sequence::get_reader;

	let iter = SAMIterator::new(get_reader(&args.sam_path)).zip(BufferedSequenceIterator::new(get_reader(&args.fq_path)));
	let mut count = 0usize;

	for (sam, seq) in iter {
		if seq.description.starts_with(&sam.qname) {
			if sam.flag & 0x10 == 0 {
				println!("{}", sam.seq);
				if sam.seq != seq.sequence {
					count += 1;
					eprintln!("find different {}", count);
				}
			}
		} else {
			eprintln!("seq:");
			eprintln!("{}", seq.to_string());
			eprintln!("sam:");
			eprintln!("{}", sam.to_string());
			panic!("SAM file and Sequence file do not match!");
		}
	}
}

#[derive(Debug)]
struct Args {
	fq_path: String,
	sam_path: String,
}

fn parse_args() -> Args {
	use clap::{App, Arg};
	let matches = App::new("Test Driver")
		.arg(Arg::with_name("fq")
			.required(true)
			.index(1))
		.arg(Arg::with_name("sam")
			.required(true)
			.index(2))
		.get_matches();

	let fq_path = matches.value_of("fq").unwrap().to_owned();
	let sam_path = matches.value_of("sam").unwrap().to_owned();

	Args { fq_path, sam_path }
}
