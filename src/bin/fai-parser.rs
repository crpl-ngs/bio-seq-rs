use std::io::{BufReader, BufRead};

fn main() {
	let args = parse_cmd();
	run(&args);
}

fn run(args: &Args) {
	for path in &args.paths {
		match std::fs::File::open(path) {
			Err(err) => {
				eprintln!("Error opening {}, with error: {}", path, err);
			},
			Ok(fp) => {
				let reader = BufReader::new(fp);
				for line in reader.lines() {
					println!("{}", line.unwrap().trim());
				}
			}
		}
	}
}

#[derive(Debug)]
struct Args {
	paths: Vec<String>,
}

fn parse_cmd() -> Args {
	let matches = clap::App::new("Parsing fai files")
		.arg(clap::Arg::with_name("paths")
			.required(true)
			.multiple(true))
		.get_matches();

	let paths = matches.values_of("paths").unwrap().map(|s| s.to_owned()).collect::<Vec<String>>();

	Args { paths }
}