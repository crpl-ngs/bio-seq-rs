use clap::{App, Arg};
use std::collections::HashMap;
use bio_seq_rs::fai::{FAIEntry, FAIIterator};
use std::io::{Seek, BufReader, Read};

fn main() {
	let args = parse_cmd();
	run(&args);
}

fn run(args: &Args) {
	let mut entries = Vec::new();
	let mut hm = HashMap::new();

	let path = format!("{}.fai", args.seq_prefix);
	let reader = std::io::BufReader::new(std::fs::File::open(&path).unwrap());
	for entry in FAIIterator::new(Box::new(reader)) {
		entries.push(entry);
	}

	for entry in &entries {
		hm.insert(&entry.name, entry);
	}

	let path = &args.seq_prefix;
	let mut fp = std::fs::File::open(path).unwrap();
	let fai = hm[&args.seq_name];

	let skipped = num_skipped(fai.linebases, fai.linewidth, args.start_location - 1);
	fp.seek(std::io::SeekFrom::Start(fai.offset + skipped + args.start_location - 1));
	let mut reader = BufReader::new(fp);
	let buf_len = args.length + num_skipped(fai.linebases, fai.linewidth, args.length as u64) as u32;
	let mut buf = Vec::new();
	buf.resize(buf_len as usize, 0);
	reader.read_exact(&mut buf);
	let mut s = String::from_utf8(buf).unwrap();
	s.retain(|c| !c.is_whitespace());
	println!("{}", s);
}

#[inline]
fn num_skipped(linebases: u32, linewidth: u32, length: u64) -> u64 {
	length / linebases as u64 * (linewidth - linebases) as u64
}

struct Args {
	seq_prefix: String,
	seq_name: String,
	start_location: u64,
	length: u32,
}

fn parse_cmd() -> Args {
	let matches = App::new("Take a slice from sequence file")
		.arg(Arg::with_name("seq_prefix").required(true))
		.arg(Arg::with_name("seq_name").required(true))
		.arg(Arg::with_name("start_location").required(true))
		.arg(Arg::with_name("length").required(true))
		.get_matches();

	let seq_prefix = matches.value_of("seq_prefix").unwrap().to_owned();
	let seq_name = matches.value_of("seq_name").unwrap().to_owned();
	let start_location = matches.value_of("start_location").unwrap().parse::<u64>().unwrap();
	let length = matches.value_of("length").unwrap().parse::<u32>().unwrap();
	Args { seq_prefix, seq_name, start_location, length }
}