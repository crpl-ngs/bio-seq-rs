use std::io::prelude::*;
use std::str::Chars;

pub fn get_reader(path: &str) -> Box<dyn BufRead> {
    use flate2::bufread::GzDecoder;
    use std::io::BufReader;

    let decoder = GzDecoder::new(BufReader::new(std::fs::File::open(path).unwrap_or_else(|_| panic!("Cannot open file: {}", path))));
    if decoder.header().is_none() {
        Box::new(BufReader::new(std::fs::File::open(path).expect("Cannot open file")))
    } else {
        Box::new(BufReader::new(decoder))
    }
}

#[derive(Debug,Default)]
pub struct Sequence {
    pub description: String,
    pub sequence: String,
    pub quality: Option<String>,
    pub display_width: usize,
}

impl Sequence {
    pub fn new_fa(desc: &str, seq: &str) -> Self {
        Sequence { description: desc.to_owned(), sequence: seq.to_owned(), quality: None, display_width: 60 }
    }

    pub fn new_fq(desc: &str, seq: &str, qual: &str) -> Self {
        assert_eq!(seq.len(), qual.len(), "Sequence length and quality length must be the same");
        Sequence { description: desc.to_owned(), sequence: seq.to_owned(), quality: Some(qual.to_owned()), display_width: 60 }
    }

    pub fn new() -> Self {
        Self::new_fa("", "")
    }

    pub fn sequence_to_partitioned_string(seq: &str, width: usize) -> String {
        let slice = seq.chars().collect::<Vec<char>>();
        let chunks = slice.chunks(width);
        chunks.map(|slice| slice.iter().collect::<String>()).collect::<Vec<String>>().join("\n")
    }
}

impl ToString for Sequence {
    fn to_string(&self) -> String {
        match &self.quality {
            Some(quality) => {
                format!("@{}\n{}\n+\n{}",
                        self.description,
                        Sequence::sequence_to_partitioned_string
                            (&self.sequence, self.display_width),
                        Sequence::sequence_to_partitioned_string
                            (quality, self.display_width)
                )
            }
            None => {
                format!(">{}\n{}", self.description,
                        Sequence::sequence_to_partitioned_string
                            (&self.sequence, self.display_width)
                )
            }
        }
    }
}

impl std::cmp::PartialEq for Sequence {
    fn eq(&self, other: &Self) -> bool {
        self.description == other.description &&
            self.sequence == other.sequence &&
            self.quality == other.quality
    }
}

struct SequenceIterator<'a> {
    buffer: Chars<'a>,
}

impl SequenceIterator<'_> {
    pub fn new(s: &str) -> SequenceIterator {
        SequenceIterator { buffer: s.chars() }
    }
}

impl std::iter::Iterator for SequenceIterator<'_> {
    type Item = Sequence;

    fn next(&mut self) -> Option<Self::Item> {
        enum State {
            Start,
            Comment,
            PreDesc,
            Desc,
            Sequence,
            PreQuality,
            Quality,
        }
        let mut s = State::Start;
        let mut seq = Sequence::new();
        let mut buffer = String::new();
        let mut previous_c = None;

        for c in &mut self.buffer {
            match s {
                State::Start => {
                    if c != ';' {
                        if c != '>' && c != '@' {
                            buffer.push(c);
                        }
                        s = State::Desc;
                    } else {
                        s = State::Comment;
                    }
                }
                State::Comment => {
                    if c == '\n' {
                        s = State::PreDesc;
                    }
                }
                State::PreDesc => {
                    if c == '>' || c == '@' {
                        s = State::Desc;
                    } else if c == ';' {
                        s = State::Comment;
                    } else {
                        panic!("Wrong FASTA or FASTQ format");
                    }
                }
                State::Desc => {
                    if c != '\n' {
                        buffer.push(c);
                    } else {
                        seq.description = buffer;
                        buffer = String::new();
                        s = State::Sequence;
                    }
                }
                State::Sequence => {
                    if c != '>' && c != '+' {
                        if c != '\n' {
                            buffer.push(c);
                        }
                    } else {
                        seq.sequence = buffer;
                        if c == '+' {
                            s = State::PreQuality;
                            buffer = String::new();
                        } else {
                            //  c == '>'
                            return Some(seq);
                        }
                    }
                }
                State::PreQuality => {
                    if c == '\n' {
                        s = State::Quality;
                    }
                }
                State::Quality => {
                    if c != '@' {
                        if c != '\n' {
                            buffer.push(c);
                        }
                    } else if previous_c != Some('\n') {
                        buffer.push(c);
                    } else {
                        seq.quality = Some(buffer);
                        return Some(seq);
                    }
                }
            }
            previous_c = Some(c);
        }

        match s {
            State::Sequence => {
                seq.sequence = buffer;
                Some(seq)
            }
            State::Quality => {
                seq.quality = Some(buffer);
                Some(seq)
            }
            _ => None
        }
    }
}


pub struct BufferedSequenceIterator {
    stream: Box<dyn BufRead>,
    line_buf: Option<String>,
}

impl BufferedSequenceIterator {
    pub fn new(stream: Box<dyn BufRead>) -> BufferedSequenceIterator {
        BufferedSequenceIterator { stream, line_buf: None }
    }
}

impl std::iter::Iterator for BufferedSequenceIterator {
    type Item = Sequence;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        let mut line = String::new();
        match &self.line_buf {
            Some(line_buf) => { line = line_buf.to_owned(); }
            None => { self.stream.read_line(&mut line).unwrap_or_default(); }
        };
        self.line_buf = None;
        enum State {
            Start,
            Desc,
            Qual,
        }
        let mut s = State::Start;
        let mut seq = Sequence::new();
        let mut buf = String::new();
        let eof;

        loop {
            match s {
                State::Start => {
                    if line.starts_with('>') || line.starts_with('@') {
                        seq.description = line[1..].trim().to_owned();
                        s = State::Desc;
                    } else if !line.starts_with(';') {
                        return None;
                    }
                }
                State::Desc => {
                    if !line.starts_with('+') && !line.starts_with('>') {
                        buf.push_str(line[0..].trim())
                    } else if line.starts_with('+') {
                        seq.sequence = buf;
                        buf = String::new();
                        s = State::Qual;
                    } else {
                        self.line_buf = Some(line.trim().to_owned());
                        seq.sequence = buf;
                        return Some(seq);
                    }
                }
                State::Qual => {
                    if line.starts_with('>') || line.starts_with('@') {
                        if buf.trim().len() < seq.sequence.len() {
                            buf.push_str(line.trim());
                        } else {
                            self.line_buf = Some(line.trim().to_owned());
                            seq.quality = Some(buf.trim().to_owned());
                            return Some(seq);
                        }
                    } else {
                        buf.push_str(line.trim());
                    }
                }
            }

            line = String::new();
            if self.stream.read_line(&mut line).is_err() || line.is_empty() {
                eof = true;
                break;
            }
        }

        if eof {
            match s {
                State::Qual => {
                    seq.quality = Some(buf);
                    return Some(seq);
                }
                State::Desc => {
                    seq.sequence = buf;
                    return Some(seq);
                }
                _ => {}
            }
        }
        None
    }
}


#[cfg(test)]
mod tests {
    use std::io::Cursor;
    use super::*;

    const NUM_LINES: i32 = 2;

    #[test]
    fn test_flate2_on_gz() {
        use std::io::prelude::*;

        let input_file = "run/hg38.fa.gz";
        let reader = get_reader(input_file);
        let mut count = 0;

        for line in reader.lines() {
            println!("{}", line.unwrap_or_default());
            count += 1;
            if count >= NUM_LINES {
                break;
            }
        }
    }

    #[test]
    fn test_flate2_on_plain() {
        use std::io::prelude::*;

        let input_file = "run/hg38.fa";
        let reader = get_reader(input_file);
        let mut count = 0;

        for line in reader.lines() {
            println!("{}", line.unwrap_or_default());
            count += 1;
            if count >= NUM_LINES {
                break;
            }
        }
    }

    #[test]
    fn test_sequence_iterator() {
        let seq = Sequence::new_fa(
            "gi|186681228|ref|YP_001864424.1| phycoerythrobilin:ferredoxin oxidoreductase",
            "MNSERSDVTLYQPFLDYAIAYMRSRLDLEPYPIPTGFESNSAVVGKGKNQEEVVTTSYAFQT\
AKLRQIRAAHVQGGNSLQVLNFVIFPHLNYDLPFFGADLVTLPGGHLIALDMQPLFRDDSAYQAKYT\
EPILPIFHAHQQHLSWGGDFPEEAQPFFSPAFLWTRPQETAVVETQVFAAFKDYLKAYLDFVEQAEA\
VTDSQNLVAIKQAQLRYLRYRAEKDPARGMFKRFYGAEWTEEYIHGFLFDLERKLTVVK",
        );
        let mut seq_str = String::new();
        seq_str.push_str(&seq.to_string());
        seq_str.push_str(&seq.to_string());
        let mut iter = SequenceIterator::new(&seq_str);
        assert_eq!(iter.next().unwrap(), seq);
        assert_eq!(iter.next().unwrap(), seq);
        assert!(iter.next().is_none());


        let seq = Sequence::new_fq(
            "chrM_2850_3381_5:0:0_10:0:0_0/1",
            "AGTACATGCTAAGACTTCACCAGTCAAAGCGAACTACTATACTCAATTGATCGAATAACTTG\
ACCAACGGAACAAGTTACCCTAGGGATAACAGCGCAATCCTATTCTAGAGTCCATATCAACAATAGG\
GTTTACGACCTCGAAGTTGGATCAGGACATCCCGATGGTGCAGCCGCTATTAAAGGTTCGTTTGTTC\
ACGATTAAAGTCCTACGTGATCTGAGTTCAGACCGGAGTAATCGAGGTCGGTTTCTATCTACTTCAC\
ATTCCTCCCAGTACGAAAGGACAAGAGAAATAAGGC",
            "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~!\"#$%&'()*+@@@@@@",
        );

        let mut seq_str = String::new();
        seq_str.push_str(&seq.to_string());
        seq_str.push_str(&seq.to_string());
        seq_str.pop();
        let mut iter = SequenceIterator::new(&seq_str);
        assert_eq!(iter.next().unwrap(), seq);
        assert_eq!(iter.next().unwrap(), seq);
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_buffered_sequence_iterator() {
        let seq = Sequence::new_fq(
            "chrM_2850_3381_5:0:0_10:0:0_0/1",
            "AGTACATGCTAAGACTTCACCAGTCAAAGCGAACTACTATACTCAATTGATCGAATAACTTG\
ACCAACGGAACAAGTTACCCTAGGGATAACAGCGCAATCCTATTCTAGAGTCCATATCAACAATAGG\
GTTTACGACCTCGAAGTTGGATCAGGACATCCCGATGGTGCAGCCGCTATTAAAGGTTCGTTTGTTC\
ACGATTAAAGTCCTACGTGATCTGAGTTCAGACCGGAGTAATCGAGGTCGGTTTCTATCTACTTCAC\
ATTCCTCCCAGTACGAAAGGACAAGAGAAATAAGGC",
            "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuv>xyz{|}~!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRST@VWXYZ[\\]^_`abcdefghijklmnopqrstuv>xyz{|}~!\"#$%&'()*+@@@@@@",
        );
        let mut seq_str = String::new();
        seq_str.push_str(&seq.to_string());
        seq_str.push_str(&seq.to_string());
        seq_str.pop();
        let cursor = Box::new(Cursor::new(seq_str.into_bytes()));
        let mut iter = BufferedSequenceIterator::new(cursor);
        assert_eq!(iter.next().unwrap(), seq);
        assert_eq!(iter.next().unwrap(), seq);
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_loading_sequences() {
        let reader = get_reader("run/hg38.fa.gz");
        for (i, mut seq) in BufferedSequenceIterator::new(reader).enumerate() {
            if i < NUM_LINES as usize {
                seq.display_width = 120;
                println!("{}", seq.to_string());
            }
        }
    }
}
